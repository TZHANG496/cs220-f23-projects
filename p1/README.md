# Project 1 (P1)

## Clarifications/Corrections

None yet.

**Find any issues?** Report to us:

- Ashwin Maran <amaran@wisc.edu>
- Kincannon Wilson <kgwilson2@wisc.edu>

## Learning Objectives

In this project, you will learn to:
* run Otter public tests,
* turn in your project using Gradescope.

## Note on Academic Misconduct:
P1 is the first project of CS220, and is the **only** project where you are **not** allowed to partner with anyone else. You **must** work on this project by yourself, and turn in your submission individually. Now may be a good time to review our [course policies](https://cs220.cs.wisc.edu/f23/syllabus.html).

## Step 1: Setup

You should have a folder `cs220` that you created for Lab-P1 under `Documents`. We're assuming you did the lab and know how to do all the Tasks we mentioned. If you're confused about any of the following steps, refer back to your lab.

#### Create a sub-folder called `p1` in your `cs220` folder
Refer to [Task 1.1](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p1#task-11-create-the-folders-for-this-lab) if you need help.  

This will store all your files related to P1. This way, you can keep files for different projects separate (you'll create a `p2` sub-folder for the next project and so on). Unfortunately, computers can crash and files can get accidentally deleted, so make sure you backup your work regularly (at a minimum, consider emailing yourself relevant files on occasion).

#### Download `p1.ipynb` and `public_tests.py` to your `p1` folder.
Refer to [Task 1.7](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p1#task-17-correctly-download-a-python-file) if you need help. You will be working on `p1.ipynb` this project. The file `public_tests.py` contains the code for testing the answers in your submission.

#### Open a terminal in your `p1` folder.
Refer to [Task 1.3-1.5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p1#task-13-open-a-terminal-emulator-aka-a-terminal).

#### Run `p1.ipynb` code using jupyter notebook
Refer to [Task 4.1](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p1#task-41-start-up-a-juptyer-notebook).
Make sure to follow the directions provided in `p1.ipynb`.

## Submission instructions
- Create a [Gradescope](https://www.gradescope.com/) login, if you haven't already created it. Invite should be available via your wisc email. **You must use your wisc email to create the Gradescope login**.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P1 assignment.
- Upload the zip file into P1 assignment.
- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available in a few minutes after your submission. You should be able to see both PASS / FAIL results for the 2 test cases and your total score, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="400">
